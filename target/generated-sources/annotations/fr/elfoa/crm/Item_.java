package fr.elfoa.crm;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.2.v20151217-rNA", date="2018-11-06T16:41:37")
@StaticMetamodel(Item.class)
public class Item_ { 

    public static volatile SingularAttribute<Item, Integer> size;
    public static volatile SingularAttribute<Item, Long> id;
    public static volatile SingularAttribute<Item, String> label;
    public static volatile SingularAttribute<Item, Integer> Weight;

}