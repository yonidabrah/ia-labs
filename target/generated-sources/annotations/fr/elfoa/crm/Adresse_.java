package fr.elfoa.crm;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.2.v20151217-rNA", date="2018-11-06T17:00:56")
@StaticMetamodel(Adresse.class)
public class Adresse_ { 

    public static volatile SingularAttribute<Adresse, String> voie;
    public static volatile SingularAttribute<Adresse, String> num;
    public static volatile SingularAttribute<Adresse, Long> id;
    public static volatile SingularAttribute<Adresse, String> cp;
    public static volatile SingularAttribute<Adresse, String> voietype;
    public static volatile SingularAttribute<Adresse, String> pays;

}