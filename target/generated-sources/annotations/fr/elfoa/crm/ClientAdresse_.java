package fr.elfoa.crm;

import fr.elfoa.crm.Adresse;
import fr.elfoa.crm.Client;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.2.v20151217-rNA", date="2018-11-06T17:04:39")
@StaticMetamodel(ClientAdresse.class)
public class ClientAdresse_ { 

    public static volatile SingularAttribute<ClientAdresse, Adresse> adresse;
    public static volatile SingularAttribute<ClientAdresse, Client> client;
    public static volatile SingularAttribute<ClientAdresse, Long> id;

}