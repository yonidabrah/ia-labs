package fr.elfoa.hello.jpa;

import fr.elfoa.crm.Adresse;
import fr.elfoa.crm.Client;
import fr.elfoa.crm.ClientAdresse;
import fr.elfoa.crm.Commande;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.util.Date;

public class CrmJPATest {

    private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("crm-database");
    private EntityManager em;
    private EntityTransaction tx;


    @Before
    public void initEntityManager() throws Exception {
        em = emf.createEntityManager();
        tx = em.getTransaction();
    }


    @After
    public void closeEntityManager() throws Exception {
        if (em != null) {
            em.close();
        }
    }

    @Test
    public void testClient(){

        Client client1 = new Client("yn@gmail.fr","nida","youssef","06874454");
        Client client2 = new Client("r@gmail.fr","nida","reda","06874454");

        tx.begin();
        em.persist(client1);
        em.persist(client2);
        tx.commit();

        Assert.assertEquals(1,client1.getId());
        Assert.assertEquals(2,client2.getId());

    }

    @Test
    public void testAdresse(){

        Adresse adresse1 = new Adresse("test1","5","france","blaise pascal","rue");
        Adresse adresse2 = new Adresse("test2","10","france","meunier","avenue");

        tx.begin();
        em.persist(adresse1);
        em.persist(adresse2);
        tx.commit();

        Assert.assertEquals(1,adresse1.getId());
        Assert.assertEquals(2,adresse2.getId());

    }

    @Test
    public void testClientAdresse(){

        Client client1 = new Client("yn@gmail.fr","nida","youssef","06874454");
        Adresse adresse1 = new Adresse("test1","5","france","blaise pascal","rue");
        ClientAdresse catalogue = new ClientAdresse(client1,adresse1);


        tx.begin();
        em.persist(client1);
        em.persist(adresse1);
        em.persist(catalogue);
        tx.commit();

        Assert.assertEquals(1,catalogue.getId());

    }


    @Test
    public void testCommande(){

        Client client1 = new Client("yn@gmail.fr","nida","youssef","06874454");
        Adresse adresse1 = new Adresse("test1","5","france","blaise pascal","rue");
        Commande commande = new Commande(new Date(),adresse1,client1);


        tx.begin();
        em.persist(client1);
        em.persist(adresse1);
        em.persist(commande);
        tx.commit();

        Assert.assertEquals(3,commande.getId());

    }
}
