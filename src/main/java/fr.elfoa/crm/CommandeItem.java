package fr.elfoa.crm;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class CommandeItem {

    @Id
    @GeneratedValue
    private long id;
    private Commande commande;
    private Item item;


    public CommandeItem(){}

    public CommandeItem(Commande commande, Item item) {
        this.commande = commande;
        this.item = item;
    }

    public Commande getCommande() {
        return commande;
    }

    public void setCommande(Commande commande) {
        this.commande = commande;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }
}
