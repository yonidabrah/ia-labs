package fr.elfoa.crm;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class ClientAdresse {

    @Id
    @GeneratedValue
    private long id;
    private Client client;
    private Adresse adresse;


    public ClientAdresse(){}

    public ClientAdresse(Client client, Adresse adresse) {
        this.client = client;
        this.adresse = adresse;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Adresse getAdresse() {
        return adresse;
    }

    public void setAdresse(Adresse adresse) {
        this.adresse = adresse;
    }
}
