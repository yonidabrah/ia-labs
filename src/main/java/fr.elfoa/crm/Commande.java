package fr.elfoa.crm;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.util.Date;

@Entity
public class Commande {

    @Id
    @GeneratedValue
    private long id;
    private Date date;
    @ManyToOne
    private Adresse adresse;
    @ManyToOne
    private Client client;

    public Commande(){}

    public Commande( Date date, Adresse adresse, Client client) {
        this.date = date;
        this.adresse = adresse;
        this.client = client;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Adresse getAdresse() {
        return adresse;
    }

    public void setAdresse(Adresse adresse) {
        this.adresse = adresse;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }
}
